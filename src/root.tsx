import React, { Component } from 'react'
import { hot } from 'react-hot-loader'
import { Router, Route } from 'react-router-dom'
import { Home, LayoutHead } from '@/containers'
import createBrowserHistory from 'history/createBrowserHistory'

const history = createBrowserHistory()
const Root = () => (
  <Router history={history}>
    <div>
      <LayoutHead history={history}/>
      <Route exact path='/' component={Home}/>
      <Route path='/calculator' component={Home}/>
      <Route path='/login' component={Home}/>
      <Route path='/about' component={Home}/>
    </div>
  </Router>
)

export default hot(module)(Root)
