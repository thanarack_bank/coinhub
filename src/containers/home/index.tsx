import React, { Component } from 'react'
import { Card, Icon, Table, Avatar, Dropdown, Menu, Button } from 'antd'

// <Props, State>
const menu = (
  <Menu className='actions-list'>
    <Menu.Item>
      <a target='_blank' rel='noopener noreferrer' href='http://www.tmall.com/'>มุมมองกราฟ</a>
    </Menu.Item>
    <Menu.Item>
      <a target='_blank' rel='noopener noreferrer' href='http://www.tmall.com/'>ข้อมูลเหรียญ</a>
    </Menu.Item>
    <Menu.Item>
      <a target='_blank' rel='noopener noreferrer' href='http://www.tmall.com/'>ประวัติราคา</a>
    </Menu.Item>
  </Menu>
)
const Actions = (e: any) => {
  console.log(e)
  return (
    <Dropdown
      overlay={menu}
      placement='bottomRight'
      trigger={[ 'click' ]}
    >
      <Icon type='down-circle-o'/>
    </Dropdown>
  )
}

class Home extends Component<any, any> {
  render () {
    const dataSource: Array<any> = [
      {
        key: '1',
        avatar_icon: (<Avatar src='https://s2.coinmarketcap.com/static/img/coins/64x64/1.png' size='small'/>),
        ranking: 1,
        coin_name: 'Bitcoin',
        price: '65,200',
        supply: '86,996,201',
        market_cap: '36,552,126',
        change: 0,
        price_grape: (<img src='/static/images/graph-test.png' className='graph-week-rank'/>),
        actions: (Actions(1))
      }, {
        key: '2',
        avatar_icon: (<Avatar src='https://s2.coinmarketcap.com/static/img/coins/64x64/1027.png' size='small'/>),
        ranking: 2,
        coin_name: 'Ethereum',
        price: '59,640',
        supply: '98,316,651',
        market_cap: '4,512,473',
        change: 0,
        price_grape: (<img src='/static/images/graph-test.png' className='graph-week-rank'/>),
        actions: (Actions(2))
      }, {
        key: '3',
        avatar_icon: (<Avatar src='https://s2.coinmarketcap.com/static/img/coins/64x64/131.png' size='small'/>),
        ranking: 3,
        coin_name: 'Dash',
        price: '12,564',
        supply: '23,431,334',
        market_cap: '5,123,554',
        change: 0,
        price_grape: (<img src='/static/images/graph-test.png' className='graph-week-rank'/>),
        actions: (Actions(3))
      }
    ]
    const columns: Array<any> = [
      {
        title: (<span><Icon type='trophy'/></span>),
        dataIndex: 'ranking',
        key: 'ranking'
      }, {
        title: '',
        dataIndex: 'avatar_icon',
        key: 'avatar_icon'
      }, {
        title: (<span><Icon type='rocket'/>เหรียญ</span>),
        dataIndex: 'coin_name',
        key: 'coin_name'
      }, {
        title: (<span><Icon type='shopping-cart'/>ราคา</span>),
        dataIndex: 'price',
        key: 'price'
      }, {
        title: (<span><Icon type='pay-circle-o'/>จำนวนเหรียญ</span>),
        dataIndex: 'supply',
        key: 'supply'
      }, {
        title: (<span><Icon type='bank'/>มูลค่าในตลาด</span>),
        dataIndex: 'market_cap',
        key: 'market_cap'
      }, {
        title: (<span><Icon type='bar-chart'/>อัตราแปรผัน (24 ชม)</span>),
        dataIndex: 'change',
        key: 'change'
      }, {
        title: (<span><Icon type='line-chart'/>กราฟราคา (7 วัน)</span>),
        dataIndex: 'price_grape',
        key: 'price_grape'
      }, {
        title: '',
        dataIndex: 'actions',
        key: 'actions'
      }
    ]
    return (
      <div className='containers'>
        <div className='warp-react'>
          <Card
            title={(<span>รายการเหรียญที่เปิดเทรด</span>)}
            className='card-box table-rank'
          >
            <Table dataSource={dataSource} columns={columns} pagination={false}/>
            <div className='zone-bottom-rank'>
              <Button type='primary' icon='calculator' size='large' className='btn-trade-now'>เริ่มเทรดตอนนี้</Button>
            </div>
          </Card>
        </div>
      </div>
    )
  }
}

export default Home
