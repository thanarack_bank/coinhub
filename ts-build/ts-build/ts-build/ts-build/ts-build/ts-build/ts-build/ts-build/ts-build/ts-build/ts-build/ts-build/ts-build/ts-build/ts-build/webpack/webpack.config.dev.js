"use strict";
const path = require('path');
const webpack = require('webpack');
const merge = require('webpack-merge');
const webpackBaseConfig = require('./webpack.config');
const HtmlWebpackPlugin = require('html-webpack-plugin');
module.exports = merge(webpackBaseConfig, {
    devtool: 'source-map',
    plugins: [
        new HtmlWebpackPlugin({
            filename: 'index.html',
            template: 'index.html',
            title: 'Title',
            inject: true,
            minify: true
        })
    ]
});
//# sourceMappingURL=webpack.config.dev.js.map