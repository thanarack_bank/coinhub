import React from 'react'
import { Link } from 'react-router-dom'
import { Row, Col, Menu } from 'antd'

interface HeaderComponentProps {
  pageRedirect: any,
  profile?: any,
  errors?: any,
  defaultSelected: string,
  selectedKeys: string
}

const HeaderTop = (props: HeaderComponentProps) => {
  const linkRed = (event: any) => props.pageRedirect(event)
  const defaultSelected = props.defaultSelected
  const selectedKeys = props.selectedKeys
  return (
    <div className='containers header-top'>
      <div className='warp-react'>
        <Row>
          <Col span={6}>
            <Link to='/'><img src='/static/images/logo.png' className='web-logo'/></Link>
          </Col>
          <Col span={28}>
            <Menu
              className='header-menu'
              theme='light'
              mode='horizontal'
              defaultSelectedKeys={[ defaultSelected ]}
              selectedKeys={[ selectedKeys ]}
              onClick={linkRed}
            >
              <Menu.Item key='home'>ราคาเหรียญ</Menu.Item>
              <Menu.Item key='calculator'>ตลาดซื้อขายไทย</Menu.Item>
              <Menu.Item key='login'>เข้าสู่ระบบ</Menu.Item>
              <Menu.Item key='about'>คอยน์ฮับคือ ?</Menu.Item>
            </Menu>
          </Col>
        </Row>
      </div>
    </div>
  )
}

export default HeaderTop
