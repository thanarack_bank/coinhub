import React, { Component } from 'react'
import ReactDom from 'react-dom'
import { AppContainer, hot } from 'react-hot-loader'
import Root from './root'
import 'antd/dist/antd.css'
import '@/theme/index.scss'

const render = (Component: any) => {
  ReactDom.render(
    <AppContainer>
      <Component/>
    </AppContainer>, document.getElementById('app'))
}

render(Root)

// Webpack Hot Module Replacement API
if (module.hot) {
  module.hot.accept('./root', () => {
    // if you are using harmony modules ({modules:false})
    render(Root)
    // in all other cases - re-require App manually
    render(require('./root'))
  })
}
