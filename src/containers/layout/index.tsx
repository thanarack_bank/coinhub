import React, { Component } from 'react'
import { Row, Col, Menu, Icon } from 'antd'

class LayoutHead extends Component<any, any> {
  constructor (props: any) {
    super(props)
    this.state = {
      defaultSelected: 'home',
      selectedKeys: ''
    }
  }

  componentDidMount () {
    this.checkPathnameMenu()
  }

  checkPathnameMenu () {
    let pathname: string = this.props.history.location.pathname
    pathname = pathname.slice(1)
    if (!pathname) pathname = this.state.defaultSelected
    this.setState({
      selectedKeys: pathname
    })
  }

  pageRedirect = (e: any) => {
    let go: string = '/'
    if (e.key === 'home' || e.key === undefined) {
      go = '/'
    } else {
      go = '/' + e.key
    }
    this.props.history.push(go)
    this.checkPathnameMenu()
  }

  render () {
    const { defaultSelected, selectedKeys } = this.state
    return (
      <div>
        <div className='containers header-top'>
          <div className='warp-react'>
            <Row>
              <Col span={6}>
                <img src='/static/images/logo-generic.png' className='web-logo' onClick={this.pageRedirect}/>
              </Col>
              <Col span={28}>
                <Menu
                  className='header-menu'
                  theme='light'
                  mode='horizontal'
                  defaultSelectedKeys={[ defaultSelected ]}
                  selectedKeys={[ selectedKeys ]}
                  onClick={this.pageRedirect}
                >
                  <Menu.Item key='home'><Icon type='pay-circle-o'/>ราคาเหรียญ</Menu.Item>
                  <Menu.Item key='market'><Icon type='shop'/>ตลาดซื้อขาย</Menu.Item>
                  <Menu.Item key='login'><Icon type='wallet'/>เข้าสู่ระบบ</Menu.Item>
                  <Menu.Item key='about'><Icon type='compass'/>คอยน์ฮับคือ ?</Menu.Item>
                </Menu>
              </Col>
            </Row>
          </div>
        </div>
        {/*<div className='containers header-top-sub'>
         <div className='warp-react'>
         <Row>
         <Col span={24}>
         ราคาเหรียญ
         </Col>
         </Row>
         </div>
         </div>*/}
      </div>
    )
  }
}

export default LayoutHead
